// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es un archivo que contiene la implementacion del
// evalposfix.
//      
//
// ===========================================================================
#include "superSt.h"
#include <vector>
#include <cstdlib>
#include <sstream>
#include <stack>

// Tokenizes (splits) the SuperString, based on the delimiter
// Parameters: delim: the delimiter
// Returns: a vector of SuperStrings that were split

vector<SuperString> SuperString::tokenize(const string & delim) {
	vector<SuperString> tokens;
	size_t p0 = 0, p1 = string::npos;
	while(p0 != string::npos) {
    	p1 = find_first_of(delim, p0);
    	if(p1 != p0) {
      		string token = substr(p0, p1 - p0);
      		tokens.push_back(token);
      	}
		p0 = find_first_not_of(delim, p1);
	}
	return tokens;
}

// Overload of the operator= for using stataments such as:
// SuperString st = static_cast<string>("3 4 5 + * 1 +");
SuperString& SuperString::operator= (string s) {
	this->assign(s);
	return *this;
}


// Overload of the operator= for using stataments such as:
// SuperString st = 209;
SuperString& SuperString::operator= (int n) {
	this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream() 
   		<< std::dec << n ) ).str());
	return *this;
}

// Overload of the constructor, allows statements such as:
// SuperString st(254);
SuperString::SuperString(int a) {
   this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream() 
   		<< std::dec << a ) ).str());
}

// Converts the SuperString to int, if possible
bool SuperString::toInt(int& i) {
   char c = this->c_str()[0];
   if(empty() || ((!isdigit(c) && (c != '-') && (c != '+')))) return false ;

   char * p ;
   i = strtol(c_str(), &p, 10) ;

   return (*p == 0) ;
}
	

// Evaluates the postfix expression.
// Returns:
//   - result: integer through which we return the result of the eval
//   - boolean return value: true if the expression is valid
bool SuperString::evalPostfix(int &result) {
	//Creates vector and separates them
   	vector<SuperString> V = tokenize(" ");
	//Creates stack for ints 
	stack<int> S;
	int n;
 
	for (int i=0; i < V.size(); i++){
		int a,b; //Used to evaluate the postfix 
		if (V[i].toInt(n))	
			S.push(n);

		else {
			if (S.empty()) return false;
			if (V[i] == "+"){
				//Sets the top to a
				a=S.top();
				S.pop(); //Removes the top element
				
				//If it becomes empty, stops the func
				if (S.empty()) return false;
				//Sets the top to b
				b=S.top(); 
				S.pop();   //Removes the top element
				S.push(b+a); //Pushes the sum of b and a
			}
			if (V[i] == "-"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b-a); //Pushes the sub of b and a
                        }
			if (V[i] == "*"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b*a); //Pushes the mult of b and a
                        }
			if (V[i] == "/"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b/a); //Pushes the div of b and a
                        }

		}
		
	}
	//If the stack contains only one element, returns it
	if (S.size()== 1){
		//Returns the value of the top element in the array
		result = S.top();
		return true;
	}
	//Else, returns false
	else 
		return false;
}


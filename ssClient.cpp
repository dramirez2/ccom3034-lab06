// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es el archivo cliente del SuperString con la
// implementacion del evalpostfix.  Si es valido la expresion,
// despliega el resultado.
//
// ====================================================================

#include <iostream>
#include "superSt.h"

using namespace std;

int main() {
	int a = 0;

	SuperString s2 = static_cast<string>("3 4 5 + * 1 +");
	if ( s2.evalPostfix(a)) {
		cout << "a: " << a << endl;
	}

	return 0;
}
